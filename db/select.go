package main

import(
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main()  {
	fmt.Println("start")
	db, err := sql.Open("mysql", "root:@/golang")
	checkErr(err)

	defer db.Close()

	rows, err := db.Query("SELECT name, age FROM persons")
	checkErr(err)

	for rows.Next() {
		var name string
		var age int

		err = rows.Scan(&name, &age)
		checkErr(err)

		fmt.Println(name)
		fmt.Println(age)
	}
}
