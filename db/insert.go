package main

import(
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main()  {
	fmt.Println("start")
	db, err := sql.Open("mysql", "root:@/golang")
	checkErr(err)

	defer db.Close()

	statement, err := db.Prepare("INSERT persons SET name = ?, age = ?")
	checkErr(err)

	res, err := statement.Exec("Sergiu", 33)
	checkErr(err)

	id, err := res.LastInsertId()
	fmt.Println(id)

}

