package main

import (
	"os"
	"fmt"
	"log"
	"io/ioutil"
	"encoding/json"
)

type Person struct {
	Name string `json:"name"`
	Address string `json:"address"`
	Age int8 `json:"age"`
}

type Persons struct{
	List []Person `json:"list"`
}

func main(){
	file, err := os.Open("parser/json/person.json")
	fmt.Println(err)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	fileData, _ := ioutil.ReadAll(file)

	var person Person

	json.Unmarshal(fileData, &person)

	fmt.Println(person.Name)
	fmt.Println(person.Address)
	fmt.Println(person.Age)

	/*------------*/
	fmt.Println("***********************")

	file, err = os.Open("parser/json/persons.json")
	fmt.Println(err)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	fileData, _ = ioutil.ReadAll(file)

	var persons Persons

	json.Unmarshal(fileData, &persons)
	fmt.Println("Persons:")
	fmt.Println(len(persons.List))
	for i := 0 ; i < len(persons.List) ; i++{

		fmt.Println(persons.List[i].Name)
		fmt.Println(persons.List[i].Address)
		fmt.Println(persons.List[i].Age)
	}


}
