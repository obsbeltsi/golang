package main

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"io/ioutil"
	"os"
)
type Currencies struct {
	XMLName xml.Name `xml:"ValCurs"`
	List []Currency `xml:"Valute"`
}

type Currency struct{
	XMLName xml.Name `xml:"Valute"`
	CharCode string `xml:"CharCode"`
	Value float32 `xml:"Value"`
}

func main() {
	response, err := http.Get("http://bnm.md/ro/official_exchange_rates?get_xml=1&date=31.12.2017")
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		var currencies Currencies
		xml.Unmarshal(contents, &currencies)
		for _, currency := range currencies.List{
			if currency.CharCode == "EUR" || currency.CharCode == "USD" || currency.CharCode == "RON" {
				fmt.Println(currency.CharCode, currency.Value)
			}
		}
	}
}