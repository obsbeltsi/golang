package main

import "fmt"

func factorial(n int64) int64 {
	if n > 1 {
		return n * factorial(n - 1)
	}else{
		return 1
	}
}

func main() {
	var x int64 = 10
	fmt.Println(x)
	fmt.Println(factorial(x))
}
