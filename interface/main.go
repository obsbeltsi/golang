package main

import (
	"math"
	"fmt"
)

type FigureMetricCalculator interface {
	area() float64
	perimeter() float64
}

type ShapeDrawer interface {
	draw()
}
/* **** RECTANGLE CLASS */
type Rectangle struct{
	width, height float64
}

//interface FigureMetricCalculator
func (r Rectangle) area() float64{
	return r.width * r.height
}

//interface FigureMetricCalculator
func (r Rectangle) perimeter() float64  {
	return 2 * r.width + 2 * r.height
}

func (r Rectangle) draw ()  {
	fmt.Println("Drawing rectangle")
}

/* **** CIRCLE CLASS */
type Circle struct {
	radius float64
}

//interface FigureMetricCalculator
func (c Circle) area() float64 {
	return math.Pi * math.Sqrt(c.radius)
}

//interface FigureMetricCalculator
func (c Circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func (c Circle) draw ()  {
	fmt.Println("Drawing circle")
}

func main(){
	rectangle := Rectangle{width: 25, height:120}
	circle := Circle{radius:123}

	shapeC := FigureMetricCalculator(circle)
	fmt.Println("Circle area: ", shapeC.area())
	fmt.Println("Circle perimeter: ", shapeC.perimeter())

	shapeC = FigureMetricCalculator(rectangle)
	fmt.Println("Rectangle area: ", shapeC.area())
	fmt.Println("Rectangle perimeter: ", shapeC.perimeter())

	shapeD := ShapeDrawer(rectangle)
	shapeD.draw()


	/*Array of figures|shapes*/
	figuresArr := [...]FigureMetricCalculator{rectangle, circle}

	for i := range figuresArr {
		fmt.Println("Area of this shape is: ", figuresArr[i].area())
	}

	shapesArr := [...]ShapeDrawer{rectangle, circle, circle}

	for j := range shapesArr {
		shapesArr[j].draw()
	}

}