package main

import "fmt"

type PC struct {
	Processor
	Ram
	Video
}

type Processor struct{
	Vendor string
	Model string
	Cores int
	Frequency int
}

type Ram struct{
	Vendor string
	Capacity int
	Frequency int
}

type Video struct{
	Vendor string
	Model string
	Capacity int
	Frequency int
}

func (pc *PC) build(pcReal PC) *PC{
	pc = &pcReal
	return pc
}

func (pc PC) run(pcM PC){
	fmt.Println("Run PC with configuration:")
	if checkMin(pc, pcM) {
		fmt.Println(pc)
	}else{
		fmt.Println("Minimal resources error")
	}
}

func checkMin(pc PC, pcM PC) bool{
	return pc.Processor.Frequency >= pcM.Processor.Frequency
}

func main() {
	/*Real PC*/
	var pcReal = PC{
		Processor:Processor{
			Vendor: "Intel",
			Model: "Core I5",
			Cores: 4,
			Frequency: 2500,
		},
	}


	/*Minimal requirements*/

	var pcMinimal = PC{
		Processor{
			Cores: 4,
			Frequency: 4000,
		},
		Ram{
			Capacity: 8192,
			Frequency: 1600,
		},
		Video{
			Capacity: 2048,
			Frequency: 899,
		},
	}

	pcReal.build(pcReal)
	pcReal.run(pcMinimal)

}